# Light Year Admin Using Iframe v4

#### 介绍
该项目是光年后台管理模板(Light Year Admin Iframe)基于bootstrap 4.4.1版本。

截图暂时不发，不弄非iframe版本的了，该项目会进行一段时间，基于bootstrap 4.4.1版本。
因4.*的版本，一些class的命名已经改变，并且布局由float改成了flex，所以项目不支持从3.7版本升级到4.4.1版本。

#### 特别感谢
- Bootstrap v4.4.1
- JQuery
- Chart.js
- perfect-scrollbar
- Bootstrap-Multitabs
- ...

#### 更新记录
2020.05.13
完成bootstrap的基础组件
